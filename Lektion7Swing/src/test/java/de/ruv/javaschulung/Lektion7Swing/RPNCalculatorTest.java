package de.ruv.javaschulung.Lektion7Swing;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Stack;

import de.ruv.javaschulung.Lektion7Swing.logic.Operator;
import de.ruv.javaschulung.Lektion7Swing.logic.RPNCalculator;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class RPNCalculatorTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public RPNCalculatorTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( RPNCalculatorTest.class );
    }

    public void testOperator() {
    	assertNotNull(Operator.ADD);
    	assertNotNull(Operator.SUBTRACT);
    	assertNotNull(Operator.MULTIPLY);
    	assertNotNull(Operator.DIVIDE);
    }
    
    public void testConstrukt() {
    	RPNCalculator rpn = new RPNCalculator();
    	assertNotNull(rpn);
    	
    	Class<? extends RPNCalculator> rpnClass = rpn.getClass();

		try {
			Field field = rpnClass.getDeclaredField("stack");
			assertNotNull(field);

			assertEquals("Datentyp", Stack.class, field.getType());

			assertTrue("Feld ist nicht private", Modifier.isPrivate(field.getModifiers()));
			
			assertEquals("java.util.Stack<java.lang.Double>", field.getGenericType().getTypeName());

			field.setAccessible(true);

			@SuppressWarnings("unchecked")
			Stack<Double> stack = (Stack<Double>) field.get(rpn);

			assertNotNull(stack);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld stack fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = rpnClass.getMethod("push", new Class[] {double.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode push(double) fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = rpnClass.getMethod("operate", new Class[] {Operator.class});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", boolean.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode operate(Operator) fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = rpnClass.getMethod("top", new Class[] {});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", double.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode top() fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = rpnClass.getMethod("clear", new Class[] {});
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode clear() fehlt oder wurde falsch implementiert!");
		}
    }
    
	@SuppressWarnings("unchecked")
	public void testPush() {
    	RPNCalculator rpn = new RPNCalculator();
    	Class<? extends RPNCalculator> rpnClass = rpn.getClass();
    	
    	Stack<Double> stack;
    	try {
			Field field = rpnClass.getDeclaredField("stack");

			field.setAccessible(true);

			stack = (Stack<Double>) field.get(rpn);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld stack fehlt oder wurde falsch implementiert!");
			return;
		}
    	
    	int i = 0;
    	double d;
    	
    	assertEquals(i++, stack.size());    	
    	
    	rpn.push(d = Math.random());    	
    	assertEquals(d, stack.lastElement());
    	assertEquals(i++, stack.size());
    	
    	rpn.push(d = Math.random());
    	assertEquals(d, stack.lastElement());
    	assertEquals(i++, stack.size());
    	
    	rpn.push(d = Math.random());
    	assertEquals(d, stack.lastElement());
    	assertEquals(i++, stack.size());
    	
    	rpn.push(d = Math.random());
    	assertEquals(d, stack.lastElement());
    	assertEquals(i++, stack.size());
    }
	
	public void testTop() {
    	RPNCalculator rpn = new RPNCalculator();
    	
    	double d;
    	
    	rpn.push(d = Math.random());    	
    	assertEquals(d, rpn.top());
    	
    	rpn.push(d = Math.random());
    	assertEquals(d, rpn.top());
    	
    	rpn.push(d = Math.random());
    	assertEquals(d, rpn.top());
    	
    	rpn.push(d = Math.random());
    	assertEquals(d, rpn.top());
    }
	
	@SuppressWarnings("unchecked")
	public void testClear() {
    	RPNCalculator rpn = new RPNCalculator();
    	Class<? extends RPNCalculator> rpnClass = rpn.getClass();
    	
    	Stack<Double> stack;
    	try {
			Field field = rpnClass.getDeclaredField("stack");

			field.setAccessible(true);

			stack = (Stack<Double>) field.get(rpn);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Feld stack fehlt oder wurde falsch implementiert!");
			return;
		}
    	
    	assertEquals(0, stack.size());
    	
    	for (int i=0; i < 100; i++) {
    		rpn.push(Math.random());
    	}
    	
    	assertEquals(100, stack.size());
    	
    	rpn.clear();
    	
    	assertEquals(0, stack.size());
    }
}
